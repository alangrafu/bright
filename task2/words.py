#!/usr/bin/env python

"""words.py - Alvaro Graves <alvaro@graves.cl> - 2015."""

import sys
import re


class Words:

    """Validate words with two repeated letters and length of 7 characters."""

    def find_words(self, filename):
        """Open file and count total number of words and total number of valid words.

        Return number of valid words and number of total words.
        """
        try:
            with open(filename, "r") as f:
                counter_all = 0
                counter_selected = 0
                for line in f:
                    l = line.strip().lower()
                    counter_all += 1
                    if self.validate_word(l):
                        counter_selected += 1
            return counter_selected, counter_all
        except:
            raise IOError

    def validate_word(self, w):
        """Check w is 7 chars long and has a repeated consecutive letter."""
        if w is None:
            return False
        return len(w) == 7 and re.match(r"^\w*(.)\1\w*$", w)

if __name__ == '__main__':
    try:
        w = Words()
        selected, total = w.find_words(sys.argv[1])
        print "Selected {} out of {} ({:.1%})".format(selected,
                                                      total,
                                                      selected/float(total))
    except:
        print sys.exc_info()
        print >> sys.stderr, "Usage: words.py WORDFILE"
        sys.exit(1)