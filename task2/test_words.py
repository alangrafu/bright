#!/usr/bin/env python
"""Test for words.py"""


import unittest
from words import Words


class WordsTest(unittest.TestCase):

    def setUp(self):
        self.good_word = "looking"
        self.empty_word = ""
        self.none_word = None
        self.bad_word = "airship"
        self.short_word = "zipper"
        self.long_word = "noosphere"
        self.multicase_word = "lOokInG"

    def test_word_good_word(self):
        w = Words()
        self.assertTrue(w.validate_word(self.good_word))

    def test_word_empty_word(self):
        w = Words()
        self.assertFalse(w.validate_word(self.empty_word))

    def test_word_none_word(self):
        w = Words()
        self.assertFalse(w.validate_word(self.none_word))

    def test_word_bad_word(self):
        w = Words()
        self.assertFalse(w.validate_word(self.bad_word))

    def test_word_short_word(self):
        w = Words()
        self.assertFalse(w.validate_word(self.short_word))

    def test_word_long_word(self):
        w = Words()
        self.assertFalse(w.validate_word(self.long_word))

    def test_word_multicase_word(self):
        w = Words()
        self.assertFalse(w.validate_word(self.multicase_word))


if __name__ == '__main__':
    unittest.main()