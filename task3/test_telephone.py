#!/usr/bin/env python
"""Test for telephone.py"""


import unittest
from telephone import Telephone


class TelephoneTest(unittest.TestCase):

    def setUp(self):
        self.good_number = "12312312323"
        self.empty_number = ""
        self.none_number = None
        self.bad_number = "1234as 23!"
        self.short_number = "12312"
        self.long_number = "345--67 12 89 376"

    def test_number_good_number(self):
        t = Telephone()
        self.assertTrue(t.validate(self.good_number) == "pass")

    def test_number_empty_number(self):
        t = Telephone()
        self.assertTrue(t.validate(self.empty_number) == "fail")

    def test_number_none_number(self):
        t = Telephone()
        self.assertTrue(t.validate(self.none_number) == "fail")

    def test_number_bad_number(self):
        t = Telephone()
        self.assertTrue(t.validate(self.bad_number) == "fail")

    def test_number_short_number(self):
        t = Telephone()
        self.assertTrue(t.validate(self.short_number) == "fail")

    def test_number_long_number(self):
        t = Telephone()
        self.assertTrue(t.validate(self.long_number) == "fail")


if __name__ == '__main__':
    unittest.main()