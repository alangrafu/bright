#!/usr/bin/env python

"""telephone.py - Alvaro Graves <alvaro@graves.cl> - 2015."""

import sys
import re


class Telephone:

    """Class to validate phone numbers."""

    def validate(self, phone_number):
        """
        Receive a phone number and validates that has the right format.

        Returns 'fail' or 'pass'
        """
        if phone_number is None:
            return "fail"
        try:
            if re.match(r"^[0-9\- ]+$", phone_number):
                only_numbers = phone_number.translate(None, ' -')
                if len(only_numbers) in range(10, 12):
                    return "pass"
            return "fail"
        except:
            print >> sys.stdout, "No number provided"

if __name__ == '__main__':
    try:
        t = Telephone()
        print t.validate(" ".join(sys.argv[1:]))
    except:
        print >> sys.stderr, "Usage: telephone.py PHONE_NUMBER"
        sys.exit(1)