#!/usr/bin/env python
"""Script for task #1 for Bright.md Alvaro Graves <alvaro@graves.cl> 2015."""
from threading import Thread, BoundedSemaphore
import time
import sys
from decimal import Decimal


class Monitor:

    """Main class that monitors the heartbeat."""

    counter = 0
    lapse = None
    semaphore = None

    def __init__(self, lapse=2.0):
        """Set lapse time."""
        self.lapse = Decimal(lapse)
        self.semaphore = BoundedSemaphore()

    def show(self):
        """Process that displays the counter."""
        t0 = Decimal(time.time())
        diff = Decimal(0)
        time.sleep(self.lapse)
        while True:
            """
            We need to account for the time that passed
            and substract it from self.lapse
            """
            self.semaphore.acquire()
            print "{}".format(self.counter)
            self.counter = 0
            self.semaphore.release()
            t1 = Decimal(time.time())
            diff = t1 - t0 - lapse
            time.sleep(self.lapse)
            t0 = t1

    def main(self):
        """Main process."""
        try:
            t = Thread(target=self.show)
            t.daemon = True
            t.start()
            while True:
                line = sys.stdin.readline().strip()
                if line != "*beat*":
                    sys.exit(0)
                self.semaphore.acquire()
                self.counter += 1
                self.semaphore.release()
        except KeyboardInterrupt:
            sys.exit(0)

if __name__ == '__main__':
    if(len(sys.argv) > 1):
        try:
            lapse = int(sys.argv[1])
        except Exception:
            print >> sys.stderr, "{} is not a valid number".format(sys.argv[1])
            sys.exit(1)
    m = Monitor(lapse)
    m.main()
