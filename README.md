## Task 1

Enter to folder `task1`

Run `./heartbeat.sh | ./heartbeat.py NUMBER_OF_SECONDS`

## Task 2

Enter to folder `task2`

Note: I assumed that case didn't matter (i.e., `lOoking` is a valid word), something that was not specified in the definition, but that for me as a user would be reasonable.

In order to run the program, execute:
```
./words.py WORD_FILE
```

To run the tests, simply run 

```
./test_words.py
```


## Task 3

Enter to folder `task3`

In order to run the program, execute:
```
./telephone.py PHONE_NUMBER
```

To run the tests, simply run 

```
./test_telephone.py
```

